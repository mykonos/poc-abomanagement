# poc-abomanagement

* `oc expose service/poc-aboverwaltung `
* `oc port-forward service/poc-aboverwaltung 8080`
* `gradle jib --image=javacook/poc-aboverwaltung`

### Troubleshooting
```
Execution failed for task ':jib'.
> com.google.cloud.tools.jib.plugins.common.BuildStepsExecutionException: Build image failed, perhaps you should make sure your credentials for 'registry-1.docker.io/javacook/poc-aboverwaltung' are set up correctly. See https://github.com/GoogleContainerTools/jib/blob/master/docs/faq.md#what-should-i-do-when-the-registry-responds-with-unauthorized for help
```
Lösung `docker login`



