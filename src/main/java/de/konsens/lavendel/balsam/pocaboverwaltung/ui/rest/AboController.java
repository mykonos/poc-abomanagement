package de.konsens.lavendel.balsam.pocaboverwaltung.ui.rest;

import de.konsens.lavendel.balsam.pocaboverwaltung.ui.model.Abo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class AboController {

    Logger logger = LoggerFactory.getLogger(AboController.class);

    @GetMapping("/abos")
    public List<Abo> all() {

        var abo1 = new Abo() {{
            id = 1234L;
            start = new Date();
            end = null;
            category = "Italiano";
        }};
        var abo2 = new Abo() {{
            id = 5678L;
            start = new Date(0);
            end = null;
            category = "Diet";
        }};
        List<Abo> result = List.of(abo1, abo2);
        List<Long> aboIds = result.stream().map(abo -> abo.id).collect(Collectors.toList());
        logger.info("AboIds:" + aboIds);
        return result;
    }
}
