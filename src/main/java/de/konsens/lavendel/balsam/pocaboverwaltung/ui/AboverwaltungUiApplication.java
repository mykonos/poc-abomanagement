package de.konsens.lavendel.balsam.pocaboverwaltung.ui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AboverwaltungUiApplication {

    public static void main(String[] args) {
        SpringApplication.run(AboverwaltungUiApplication.class, args);
    }

}
