package de.konsens.lavendel.balsam.pocaboverwaltung.ui.model;

import java.util.Date;

public class Abo {

    public Long id;
    public Date start;
    public Date end;
    public String category;

}
